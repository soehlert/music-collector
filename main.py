#! /usr/bin/env python3

import os
import re
import slack

from pathlib import Path
from dotenv import load_dotenv
from flask import Flask
from slackeventsapi import SlackEventAdapter

env_path = Path('.') / '.env'
load_dotenv(dotenv_path=env_path)

app = Flask(__name__)
slack_event_adapter = SlackEventAdapter(os.environ['SIGNING_SECRET'], '/slack/events', app)
client = slack.WebClient(token=os.environ['SLACK_TOKEN'])
BOT_ID = client.api_call('auth.test')['user_id']

MUSIC_LINKS = ["youtube.com", "youtu.be", "soundcloud.com", "bandcamp.com"]


def extract_urls(slack_message):
	"""
	Takes in a message with one of the accepted urls in it and pulls out just the urls
	inputs: text string with embedded url
	outputs: url string
	"""

	urls = re.findall('https?://(?:[-\w.]|(?:%[\da-fA-F]{2}))+/.\S*', slack_message)

	return urls


@slack_event_adapter.on('message')
def message(payload):
	event = payload.get('event', {})
	channel_id = event.get('channel')
	user_id = event.get('user')
	text = event.get('text')

	if BOT_ID != user_id:
		for link in MUSIC_LINKS:
			if link in text:
				links = extract_urls(text)

	print(links)
	return links


if __name__ == "__main__":
	app.run(debug=True, port=5001)
